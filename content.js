const SPEED_INCREMENTS = 1 / 4;

function set_speed(speed) {
    $(ytspeedup).text($(".html5-main-video")[0].playbackRate = speed);
    console.log("[YT Speedup/info]: set speed to: " + get_speed());
}

function get_speed() {
    return $(".html5-main-video")[0].playbackRate;
}

setTimeout(() => {
    console.log($("video"));

    let ytspeedup = $("<div>", {
        id: "ytspeedup",
        text: get_speed()
    });
    ytspeedup.css({
        "position": "fixed",
        "left": 0,
        "bottom": 0,
        "z-index": 9999,
        "background": "white",
        "padding": ".5rem",
        "font-size": "1.2rem",
        "border-radius": "0 2px 2px 0",
        "margin-bottom": "1.5rem",
    });
    $("body").append(ytspeedup);

    document.onkeydown = function (e) {
        if (e.key == "<") {
            if (get_speed() > SPEED_INCREMENTS) { set_speed(get_speed() - SPEED_INCREMENTS); }
        }
        else if (e.key == ">") set_speed(get_speed() + SPEED_INCREMENTS);
        else if (e.altKey && e.key == "j") set_speed(1.);
        else if (e.altKey && e.key == "k") set_speed(2.);
        else if (e.altKey && e.key == "l") set_speed(10.);
    }

    console.log("[YT Speedup/info]: initialized");
}, 500);